## **HMS Bundled Services - Sample Request and Response sets** ##

These are sample request and response XML sets for the two HealthLink webservices that will be used to assist in downloading and installing the HMS Client.  Also included are the XML schemas used to validate input and output.

The first web service, the New Messaging Address web service, consumes organisation data and produces a messaging address to be used by the HMS Client Installer.  The second web service, the HMS Auto Install Web Service, consumes information produced by the first web service as well as additional installation information and produces a varfile which is used by the HMS Client Installer to configure the installation.

Please refer to the [documentation](/documentation) for further information.